## aospa_oneplus8-user 12 SKQ1.211006.001 eng.offain.20211129.001413 test-keys
- Manufacturer: oneplus
- Platform: kona
- Codename: oneplus8
- Brand: OnePlus
- Flavor: aospa_oneplus8-user
- Release Version: 12
- Id: SKQ1.211006.001
- Incremental: eng.offain.20211129.001413
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SD1A.210817.019.C2/7738411:user/release-keys
- OTA version: 
- Branch: aospa_oneplus8-user-12-SKQ1.211006.001-eng.offain.20211129.001413-test-keys
- Repo: oneplus_oneplus8_dump_8344


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
